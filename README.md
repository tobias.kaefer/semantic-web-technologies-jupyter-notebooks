# Semantic Web Technologies
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kit.edu%2Ftobias.kaefer%2Fsemantic-web-technologies-jupyter-notebooks.git/115e36329bf9a5958329e5805755d1236a0e8b55)

Jupyter notebooks for the lecture "Semantic Web Technologies". 

This lecture is offered by the [Web Science Research Group](http://www.aifb.kit.edu/web/Web_Science) at KIT. 

## Content
The following notebooks are available in the `notebooks` folder: 

* N00 Introduction to Jupyter Notebooks
* N01 Foundations of the Web 
* N02 Linked Data Basics 
* N03 Resource Description Framework (RDF)
* N04 Querying RDF Data with SPARQL
* N05 SPARQL Query Evaluation 

## License
The contents of this repository are licensed for reuse under [Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](http://creativecommons.org/licenses/by-nc/4.0/)

## Contact
For questions or comments, please contact Dr. Maribel Acosta (`maribel.acosta@kit.edu`). 
