{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linked Data Basics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will learn about the concepts associated with the Linked Data principles. \n",
    "\n",
    "This notebook is structured as follows:\n",
    "* [Linked Data Principle 1](#Linked_Data_Principle_1)\n",
    "* [Linked Data Principle 2](#Linked_Data_Principle_2)\n",
    "* [Linked Data Principle 3](#Linked_Data_Principle_3)\n",
    "* [Linked Data Principle 4](#Linked_Data_Principle_4)\n",
    "* [Exercises](#Exercises2)\n",
    "\n",
    "To handle URIs, we are going to use the `urllib.parse` Python 3 library. If you are using Python 2, change the import line as follows `import urlparse`.\n",
    "\n",
    "To handle HTTP requests, we are going to use the `requests` library. For more details about this library, please check the notebook *Hypertext, the Internet, and the Web*. \n",
    "\n",
    "To handle RDF data, we are going to use the `rdflib` library https://rdflib.readthedocs.io. To install this library execute the following command in the terminal `conda install -c conda-forge rdflib`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import urllib.parse\n",
    "import requests\n",
    "import rdflib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Linked_Data_Principle_1'></a> \n",
    "## Linked Data Principle 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">Use URIs to name things.\n",
    "\n",
    "To follow this principle, we need to use **valid URIs** to name things. The syntax of valid URIs is depicted in the following diagram. \n",
    "\n",
    "![URI Syntax Diagram](https://upload.wikimedia.org/wikipedia/commons/9/96/URI_syntax_diagram.png)\n",
    "Source: Ennex2, https://commons.wikimedia.org/wiki/File:URI_syntax_diagram.png ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following steps, we are going to perform a basic verification for valid URIs by checking that the URI has the mandatory fields: **scheme** and **path**.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the string stored in the variable `uri1`, we are going to parse it using the `urlparse` function to check whether it is a valid URI. We will store the result of parsing `uri1` in the variable `parsed_uri1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri1 = \"http://people.aifb.kit.edu/yt2652/swt/data/nobel.ttl#Marie_Curie\"\n",
    "parsed_uri1 = urllib.parse.urlparse(uri1)\n",
    "parsed_uri1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous output, we can see the result of parsing the string. The string is decomposed into the different URI  fields. The userinfo, host, and port compose the `netloc` (or authority) field.\n",
    "\n",
    "Next, we verify that the URI has the mandatory fields scheme and path. To do so, we use the function `all` which evaluates to `True` when all the elements of a given list are non-empty."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all([parsed_uri1.scheme, parsed_uri1.path])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The verification returns `True`, therefore, we conclude that `uri1` is a **valid URI** and adheres to the Linked Data Principle 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets try the same verfifcation process on another string stored in `uri2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri2 = \"urn:isbn:9-78284-0116462\"\n",
    "parsed_uri2 = urllib.parse.urlparse(uri2)\n",
    "all([parsed_uri1.scheme, parsed_uri1.path])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The verfication returns `True`, therefore, we conclude that `uri2` is also **a valid URI** and adheres to the Linked Data Principle 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets try the same verfifcation process on another string stored in `uri3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri3 = \"http//ricin.im//user/SkyzohKey?id=3\"\n",
    "parsed_uri3 = urllib.parse.urlparse(uri3)\n",
    "parsed_uri3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous output, we can see that the parsed URI does not have a scheme. We can automatically verify this with the following step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all([parsed_uri3.scheme, parsed_uri3.path])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The verfication returns `False`, therefore, we conclude that `uri3` is **not a valid URI**. The syntax of this URI is invalid as the `:` after the scheme is missing.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Linked_Data_Principle_2'></a> \n",
    "## Linked Data Principle 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">Use HTTP URIs so that people can look up those names."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To verify if a URI adheres to this principle, we are going to first check whether a given URI is an **HTTP or HTTPS URI**. Then, we are going to check if the URI is also **dereferenceable**. \n",
    "\n",
    "In this section of the notebook, we are going to check only the valid URIs from the previous section (i.e., `uri1` and `uri2`). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri1 = \"http://people.aifb.kit.edu/yt2652/swt/data/nobel.ttl#Marie_Curie\"\n",
    "parsed_uri1 = urllib.parse.urlparse(uri1)\n",
    "parsed_uri1.scheme == \"http\" or parsed_uri1.scheme == \"https\"  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The URI corresponds to an HTTP/S URI, therefore, we will verify that URI can be looked up (or dereferenced). To check if the URI is dereferenceable, we will perform an HTTP GET request with the `requests` library and check that the status code of the reponse is `200`. Note that by default, the `get` function follows the redirects automatically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response = requests.get(uri1)\n",
    "response.status_code == 200"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can conclude that `uri1` is an **HTTP and dereferenceable URI**. Therefore, `uri1` adheres to the Linked Data Principle 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets now perform the same verification on `uri2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri2 = \"urn:isbn:9-78284-0116462\"\n",
    "parsed_uri2 = urllib.parse.urlparse(uri2)\n",
    "parsed_uri2.scheme == \"http\" or parsed_uri2.scheme == \"https\"  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, `uri2` is not an HTTP URI. Therefore, `uri2`does not adhere to the Linked Data Principle 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Linked_Data_Principle_3'></a> \n",
    "## Linked Data Principle 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">When someone looks up a URI, provide *useful* information, using the standards (RDF*, SPARQL)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To verify if a URI adheres to the Linked Data Principle 3, we are going to check whether the data obtained from dereferencing the URI is published in one of the RDF serializations (e.g., RDF/XML, N-triples, Turtle, etc.). To do this, we are going to dereference the URI with the `get` function from the `requests` library. Then, we inspect the `Content-Type` field of the response header.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri1 = \"http://people.aifb.kit.edu/yt2652/swt/data/nobel.ttl#Marie_Curie\"\n",
    "response = requests.get(uri1)\n",
    "response.headers['Content-Type']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The header of the response indicates that the data is modelled as Turtle, which complies with the standards (RDF*).   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can load the dereferenced data into an RDF graph using the `rdflib` library.  First, we create an empty RDF graph. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = rdflib.Graph()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we load the data from `uri1` into the previously created empty graph. We have to specify the serialization of the data. In this case, we use `ttl` to specify that the data is represented in the Turtle format. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g.load(uri1, format=\"ttl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `len` applied to an RDF graph returns the number of RDF triples that were loaded into the graph. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(g)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also access the retrieved RDF triples by iterating over the elements of the graph as follows. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for (s,p,o) in g:\n",
    "    print(s,p,o, \"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we have dereferenced RDF data from `uri1`, we can conclude that `uri1` adheres to the Linked Data Principle 3. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets now consider another example with `uri4`.  First, we are going to look up the URI and determine the format of the dereferenced data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri4 = \"http://people.aifb.kit.edu/yt2652/swt/data/awards.ttl\"\n",
    "response = requests.get(uri4)\n",
    "response.headers['Content-Type']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The retrieved data is represented in the Turtle format. Next, we load the data into another empty RDF graph. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g2 = rdflib.Graph()\n",
    "g2.load(uri4, format=\"ttl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we checkif we retrieved information when dereferencing the URI. To do this, we count the number of RDF triples loaded into the graph. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(g2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The URI `uri4` did not provide information in RDF. Therefore, `uri4` does not adhere to the Linked Data Principle 3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Linked_Data_Principle_4'></a> \n",
    "## Linked Data Principle 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">Include links to other URIs, so that they can discover more things."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To verify if links to other URIs have been included in the RDF data, we are going to check the syntax of the URIs used in the dereferenced data. For the purpose of this lecture, we are going to define **external URIs** as URIs served in a different source, i.e., URIs with diffferent authority part.    \n",
    "\n",
    "Lets consider an example when dereferecing `uri1`. To extract the authority part of `uri1`, we parse the URI with `urlparse` and then access the attribute `netloc`.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri1 = \"http://people.aifb.kit.edu/yt2652/swt/data/nobel.ttl#Marie_Curie\" \n",
    "parsed_uri1 = urllib.parse.urlparse(uri1)\n",
    "parsed_uri1.netloc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we store the dereferenced data from `uri1` into an RDF graph.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = rdflib.Graph()\n",
    "g.load(uri1, format=\"ttl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are going to perform a simple verification by checking whether some of the RDF triples from `uri1` use external URIs as follows: \n",
    "\n",
    "1. We iterate over the RDF triples of the graph. \n",
    "2. For each triple, we apply the function `isinstance` to each term of the triple. We use this function to check if a term corresponds to a URI. \n",
    "3. In the case of URIs, we check that the authority of the term and the authority of the dereferenced URI are not the same.  \n",
    "4. If 2. and 3. are fulfilled, then the URI of the RDF term is an **external URI** . \n",
    "\n",
    "**Important.** Please note that the following verification is not exhaustive. For example, the case when external URIs for datatypes are used in literals is not captured.   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "external_uri = False\n",
    "for rdf_triple in g:\n",
    "    for term in rdf_triple:\n",
    "        if isinstance(term, rdflib.URIRef) and urllib.parse.urlparse(term).netloc != parsed_uri1.netloc:\n",
    "            external_uri = True\n",
    "print(external_uri)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simple verification process returns `True`, which means that the dereferenced data contains links to other URIs. Therefore, we can conclude that `uri1` adheres to the Linked Data Principle 4. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Exercises2'></a>\n",
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.** The following blocks of code return `True` when the given URIs adhere to the Linked Data Principle 1. Complete the code accordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri5 = \"http;//dbpedia.org/data/Marie_Curie.n3\"\n",
    "parsed_uri5 = urllib.parse.urlparse(...)\n",
    "all([parsed_uri5...., parsed_uri5....])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri6 = \"ftp://people.aifb.kit.edu/yt2652/swt//data/nobel.ttl\"\n",
    "parsed_uri6 = urllib.parse.urlparse(...)\n",
    "all([...])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri7 = \"http://dbpedia.org/data/Marie_Curie.n3\"\n",
    "parsed_uri7 = ...\n",
    "all([...])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.**  The following blocks of code return `True` when the given URIs adhere to the Linked Data Principle 2. Complete the code accordingly. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri6 = \"ftp://people.aifb.kit.edu/yt2652/swt//data/nobel.ttl\"\n",
    "parsed_uri6 = urllib.parse.urlparse(...)\n",
    "if parsed_uri6.scheme == ... or parsed_uri6.scheme == \"https\":\n",
    "    print(requests.get(...).status_code == ...)\n",
    "else:\n",
    "    print(False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri7 = \"http://dbpedia.org/data/Marie_Curie.n3\"\n",
    "parsed_uri7 = urllib.parse.urlparse(...)\n",
    "if ... :\n",
    "    print(requests.get(...).status_code == ...)\n",
    "else:\n",
    "    print(False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri8 = \"http://people.aifb.kit.edu/yt2652/swt/data/universities.ttl\"\n",
    "parsed_uri8 = urllib.parse.urlparse(uri8)\n",
    "if ... :\n",
    "    print(...)\n",
    "else:\n",
    "    print(...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 3.** Complete the following blocks of code to determine if the given URIs adhere to the Linked Data Principle 3. First, make sure that the URIs provide information using the standards. Then, check whether the URIs actually provide information (RDF triples). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri7 = \"http://dbpedia.org/data/Marie_Curie.n3\"\n",
    "response7 = requests.get(...)\n",
    "response7.headers['Content-Type']\n",
    "# Uncomment and complete the following code if the URI provides information using the standard.\n",
    "# g7 = rdflib.Graph()\n",
    "# g7.load(..., format=\"ttl\")\n",
    "# len(...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri9 = \"http://people.aifb.kit.edu/yt2652/swt/data/grades.csv\"\n",
    "response9 = requests.get(...)\n",
    "response9....\n",
    "# Uncomment and complete the following code if the URI provides information using the standard.\n",
    "# g9 = rdflib.Graph()\n",
    "# g9.load(..., format=\"ttl\")\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 4 (Difficult).** Consider the following RDF document in turtle syntax specified in the variable `uri_turing`. The dereferenced RDF triples have been loaded into the graph `g_turing`.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri_turing = \"http://people.aifb.kit.edu/yt2652/swt/data/turing.ttl\"\n",
    "g_turing = rdflib.Graph()\n",
    "g_turing.load(uri_turing, format=\"ttl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your task is to complete the following blocks of code to determine whether the given RDF document adheres to the Linked Data principles. The verification process is as follows:  \n",
    "\n",
    "* For Linked Data Principle 1: check whether the URIs used in the RDF triples of the document are valid URIs. \n",
    "* For Linked Data Principle 2: check whether the URIs used in the RDF triples of the document are HTTP/S URIs and are also dereferenceable. \n",
    "* For Linked Data Principle 3: check whether the RDF graph `g_turing` provides information in RDF, i.e., check that the number of retrieved RDF triples is larger than zero.  \n",
    "* For Linked Data Principle 4: check whether the URIs used in the RDF triples of the document are external URIs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Verification of Linked Data Principle 1\n",
    "verifications1 = []\n",
    "for rdf_triple in g_turing:\n",
    "    for term in rdf_triple:\n",
    "        if isinstance(term, rdflib.URIRef):\n",
    "            parsed_term = urllib.parse.urlparse(term)\n",
    "            # Verification of valid URI\n",
    "            verifications1.append(all([...]))\n",
    "            \n",
    "print(\"The document adheres to the Linked Principle 1:\", all(verifications1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Verification of Linked Data Principle 2\n",
    "verifications2 = []\n",
    "for rdf_triple in g_turing:\n",
    "    for term in rdf_triple:\n",
    "        if isinstance(term, rdflib.URIRef):\n",
    "            parsed_term = urllib.parse.urlparse(term)\n",
    "            # Verification of HTTP/S URIs\n",
    "            verifications2.append(parsed_term.scheme == ... or parsed_term.scheme == ...)\n",
    "            # Verification of dereferenceable URIs \n",
    "            verifications2.append(requests.get(term).... == 200)\n",
    "            \n",
    "print(\"The document adheres to the Linked Principle 2:\", all(verifications2))  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Verification of Linked Data Principle 3\n",
    "print(\"The document adheres to the Linked Principle 3:\", len(...) > ...) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Verification of Linked Data Principle 4\n",
    "verifications4 = []\n",
    "uri_turing_parsed = urllib.parse.urlparse(uri_turing)\n",
    "for rdf_triple in g_turing:\n",
    "    for term in rdf_triple:\n",
    "        if isinstance(..., rdflib.URIRef):\n",
    "            parsed_term = urllib.parse.urlparse(term)\n",
    "            # Verification of HTTP/S URIs\n",
    "            verifications4.append(... != uri_turing_parsed.netloc)\n",
    "    \n",
    "print(\"The document adheres to the Linked Principle 4:\", all(verifications4))  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_metadata": {
   "author": "Maribel Acosta",
   "title": "Linked Data Basics - Jupyter Notebook",
   "year": "2019"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
