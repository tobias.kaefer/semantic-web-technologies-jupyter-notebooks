{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Resource Description Framework (RDF)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will learn about the concepts associated with the Resource Descriptiion Framework (RDF). \n",
    "\n",
    "This notebook is structured as follows:\n",
    "* [RDF Triple](#RDF_Triple)\n",
    "* [Blank Nodes](#Blank_Nodes)\n",
    "* [Literals](#Literals)\n",
    "* [RDF Graphs](#RDF_Graphs)\n",
    "* [Exercises](#Exercises3)\n",
    "\n",
    "To handle RDF data, we are going to use the `rdflib` library https://rdflib.readthedocs.io. To install this library execute the following command in the terminal `conda install -c conda-forge rdflib`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import rdflib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='RDF_Triple'></a> \n",
    "## RDF Triple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "RDF defines a graph-based data model, where the nodes of the graph represent entities/values and the edges represent the relationships among them. \n",
    "\n",
    "In RDF, the basic unit to represent entities and relationships is the **RDF triple**. Each triple is of the form `subject predicate object .`.   \n",
    "\n",
    "For example, the following RDF triple (in [N-Triples](https://www.w3.org/TR/n-triples/)) states that *Marie Curie discovered Radium*:\n",
    "\n",
    "$<http://example.org/science#Marie_Curie> <http://example.org/science#discovered> <http://example.org/science#Radium> .$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To create an RDF triple using `rdflib`, we first create each of the **RDF terms** used in the triple. The RDF terms are:\n",
    "* **URIs**: used to globally identify resources. To create a URI in RDFLib we use `rdflib.URIRef`\n",
    "* **Blank nodes**: refer to resources without identifiers and not globally addressable. To create a blank node in RDFLib we use `rdflib.BNode` \n",
    "* **Literals**: concrete data values (strings, integers, floats, dates, etc.). To create a literal in RDFLib we use `rdflib.Literal` \n",
    "\n",
    "For example, in the following we create the RDF terms for the given RDF triple about Marie Curie.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mcurie = rdflib.URIRef('http://example.org/science#Marie_Curie')\n",
    "discovered = rdflib.URIRef('http://example.org/science#discovered')\n",
    "radium = rdflib.URIRef('http://example.org/science#Radium')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can create an RDF triple by creating a tuple structure in Python, using the previously defined subject (variable `curie`), predicate (variable `discovered`), and object (variable `radium`). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "triple = (mcurie, discovered, radium)\n",
    "triple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Formally, RDF triples can be defined as a 3-tuple $<s,p,o> \\ \\in \\ (U \\cup B) \\times U \\times (U \\cup B \\cup L)$, where $U$ is the set of URIs, $B$ the set of blank nodes, and $L$ the set of RDF literals. This means that:\n",
    "* **Subjects** can be URIs or blank nodes\n",
    "* **Predicates** can only be URIs \n",
    "* **Objects** can be URIs, blank nodes, or RDF literals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, we have learned how to create triples with URI terms. In the following sections, we will learn more about [blank nodes](#Blank_Nodes) and [literals](#Literals)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Blank_Nodes'></a> \n",
    "## Blank Nodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Blank nodes (or bnodes) are RDF terms that refer to resources without a known identifier. Formally, blank nodes represent *existential variables*, i.e., they indicate that the resource exists but the concrete value is not given.  \n",
    "\n",
    "For example, assume that we want to model the statement *Marie Curie discovered something*, but we do not know exactly what the discovery was. We can use a blank node in the object position of the triple to indicate that the value of the discovery is, in this example, unknown:\n",
    "\n",
    "$\n",
    "<http://example.org/science#Marie_Curie> <http://example.org/science#discovered> _:x . \n",
    "$\n",
    "\n",
    "To represent blank nodes in the [N-Triples](https://www.w3.org/TR/n-triples/) serialization, we use the prefix `_:` before the label of the blank node.  \n",
    "\n",
    "**Important.** If the same blank node label (for example, `_:x`) appears in different RDF graphs, they do not represent the same resource. This is what we mean when we say that blank nodes are not *globally addressable*.  \n",
    "\n",
    "In RDFLib, we create a blank node using the function `rdflib.BNode`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "discovery = rdflib.BNode()\n",
    "discovery"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that, in this example, RDFLib assigned an internal label or hash to the blank node. \n",
    "\n",
    "**Important.** RDFLib allows for specifying a label for the blank node. However, this functionality should only be used by query engines and not when we are creating the RDF terms and triples manually. \n",
    "\n",
    "Now we can use the blank node `discovery` in an RDF triple to model that *Marie Curie discovered something*. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "triple2 = (mcurie, discovered, discovery)\n",
    "triple2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we can reuse the same blank node in another RDF triple. For example, we can model that *Pierre Curie discovered something* and that something is the same resource discovered by Marie Curie, as follows:\n",
    "\n",
    "$<http://example.org/science#Pierre_Curie> <http://example.org/science#discovered> _:x .$ \n",
    "\n",
    "In this case, we just need to create an URI term for Pierre Curie, and reuse the terms in the variables `discovered` and `discovery` that we previously created.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcurie = rdflib.URIRef('http://example.org/science#Pierre_Curie')\n",
    "triple3 = (pcurie, discovered, discovery)\n",
    "triple3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Literals'></a> \n",
    "## Literals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Literals allow for specifying values (strings, numbers, dates, etc.) in RDF. \n",
    "\n",
    "For example, we use a literal to model the statement *Marie Curie's birthname is Maria Salomea Skłodowska*, as follows:\n",
    "\n",
    "$<http://example.org/science#Marie_Curie> <http://example.org/science#birthname> \"Maria Salomea Skłodowska\" .$\n",
    "\n",
    "In the [N-Triples](https://www.w3.org/TR/n-triples/) serialization, literals are enclosed in quotes. \n",
    "\n",
    "To create a literal in RDFLib, we use the function `rdflib.Literal`.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = rdflib.Literal(\"Maria Salomea Skłodowska\")\n",
    "name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous literal is considered a *simple* literal. In RDF, it is possible to create literals annotated with [language tags](#Language-tagged_Literals) and [datatypes](#Datatyped_Literals).  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Language-tagged_Literals'></a> \n",
    "### Language-tagged Literals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In some cases, it is necessary to specify the language (English, German, etc.) of a literal value.\n",
    "\n",
    "In the [N-Triples](https://www.w3.org/TR/n-triples/) serialization, we specify the language tag preceded by '@'. \n",
    "For example, the following RDF triple models the statement *Marie Curie's thesis title is \"Recherches sur les substances radioactives\" (in French)*: \n",
    "\n",
    "$<http://example.org/science#Marie_Curie> <http://example.org/science#thesisTitle> \"Recherches sur les substances radioactivesl\"@fr .$\n",
    "\n",
    "The valid language tags are specified in the document [IETF Current Best Practices - Tags for Identifying Languages](https://tools.ietf.org/html/bcp47). In this notebook, we will use language tags for English (`en`), German (`de`), Spanish (`es`), and French (`fr`) literals.  \n",
    "\n",
    "To create a language-tagged literal in RDFLib, we use the function `rdflib.Literal`, and specify the corresponding language in the `lang` input parameter.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "title_fr = rdflib.Literal(\"Recherches sur les substances radioactives\", lang='fr')\n",
    "title_fr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** Two RDF literals with the same lexical form (value) but different language tags are considered different.  \n",
    "\n",
    "For example, consider the following comparison between the variable `title_fr` and an RDF literal with the same value as `title_fr` but with an English language tag (omit the fact that it is incorrect to say that the given value is a valid text in Engish).  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "title_en = rdflib.Literal(\"Recherches sur les substances radioactives\", lang='en')\n",
    "title_fr == title_en"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The comparison confirms that the two RDF literals are different, because they have different language tags. \n",
    "\n",
    "Similarly, if we compare a language-tagged literal with a simple literal with the same lexical form, we will obtain that the two literals are different. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "title_simple = rdflib.Literal(\"Recherches sur les substances radioactives\")\n",
    "title_fr == title_simple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Datatyped_Literals'></a> \n",
    "### Datatyped Literals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In RDF, it is possible to specify the datatype (integer, float, date, year, etc.) of a literal. \n",
    "\n",
    "In the [N-Triples](https://www.w3.org/TR/n-triples/) serialization, we specify the datatype preceded by '^^'. \n",
    "For example, the following RDF triple models the statement *Marie Curie's birthdate is \"1867-11-07\" (date)*: \n",
    "\n",
    "$<http://example.org/science#Marie_Curie> <http://example.org/science#birthDate> \"1867-11-07\"^^<http://www.w3.org/2001/XMLSchema#date> .$\n",
    "\n",
    "The valid datatypes are defined in the [XML Schema 1.1 Part 2: Datatypes](http://www.w3.org/TR/xmlschema11-2/). In this notebook, we will use `xsd:boolean`, `xsd:date`, `xsd:integer`, `xsd:float`, and `xsd:gYear`. \n",
    "\n",
    "To create a datatyped literal in RDFLib, we use the function `rdflib.Literal`, and specify the corresponding datatype in the `datatype` input parameter.  In addition, we can use the `rdflib.XSD` namespace instead of writing the full URI of the datatype. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "date = rdflib.Literal(\"1867-11-07\", datatype=rdflib.XSD.date)\n",
    "date"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** Two RDF literals with the same lexical form (value) but different datatypes are considered different.  \n",
    "\n",
    "For example, consider the following comparison between two literals (`literal1` and `literal2`) witht the same lexical form but different datatypes. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "literal1 = rdflib.Literal(\"1\", datatype=rdflib.XSD.integer)\n",
    "literal2 = rdflib.Literal(\"1\", datatype=rdflib.XSD.float)\n",
    "literal1 == literal2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** The RDF specification defines that two RDF literals with the same datatype, different lexical form but with a lexical form that represents the same value are also considered different.  However, RDFLib does not follow this definition by default. To ensure that RDFLib follows the RDF specification, we need to specify `normalize=False` when creating the literal. \n",
    "\n",
    "For example, consider the following comparison between two literals (`literal3` and `literal4`) with the different lexical forms that represents the same value (in this case, the number 1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "literal3 = rdflib.Literal(\"01\", datatype=rdflib.XSD.integer, normalize=False)\n",
    "literal4 = rdflib.Literal(\"1\", datatype=rdflib.XSD.integer, normalize=False)\n",
    "literal3 == literal4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary on Literal Term Equality"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two literals are term-equal (the same RDF literal) if and only if the two lexical forms, the two datatype IRIs, and the two language tags (if any) compare equal, character by character. Thus, two literals can have the same value without being the same RDF term."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='RDF_Graphs'></a> \n",
    "## RDF Graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An RDF graph is a set of RDF triples. To create an RDF graph in RDFLib, we use the function `rdflib.Graph`. This function creates an empty graph.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g1 = rdflib.Graph()\n",
    "g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can add triples to the graph with the function `add` as follows:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Creation of RDF Terms\n",
    "mcurie = rdflib.URIRef('http://example.org/science#Marie_Curie')\n",
    "discovered = rdflib.URIRef('http://example.org/science#discovered')\n",
    "radium = rdflib.URIRef('http://example.org/science#Radium')\n",
    "# Creation of an RDF triple (optional)\n",
    "triple = (mcurie, discovered, radium)\n",
    "# Adding the triple to the graph\n",
    "g1.add(triple)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now add another triple that we created in the previous sections. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g1.add(triple2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To access the content of an RDF graph, we can iterate over the triples. For example: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for t in g1:\n",
    "    print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since RDF graphs are sets of triples, typical set operations are supported such as union, intersection, and difference. \n",
    "\n",
    "To illustrate these operations, in the following we create another RDF graph `g2` and add some triples that were created in previous sections. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g2 = rdflib.Graph()\n",
    "g2.add(triple2)\n",
    "g2.add(triple3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Union of RDF Graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The union of two RDF graphs $G_1$ and $G_2$ corresponds to the RDF triples that belong either to $G_1$ or to $G_2$.  \n",
    "\n",
    "In the union of RDF graphs, triples in common in both graphs appear only once.\n",
    "\n",
    "To perform the union of two RDF graphs created with RDFLib, we use the set operator `|` of Python. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "union_graph = g1 | g2\n",
    "for t in union_graph:\n",
    "    print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Intersection of RDF Graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The union of two RDF graphs $G_1$ and $G_2$ corresponds to the RDF triples that $G_1$ and $G_2$ have in common. \n",
    "\n",
    "To perform the intersection of two RDF graphs created with RDFLib, we use the set operator `&` of Python. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "intersection_graph = g1 & g2\n",
    "for t in intersection_graph:\n",
    "    print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Difference of RDF Graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The difference of two RDF graphs $G_1$ and $G_2$ corresponds to the RDF triples that belong to $G_1$ and do not belong to $G_2$. \n",
    "\n",
    "To perform the difference of two RDF graphs created with RDFLib, we use the set operator `-` of Python. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "difference_graph = g1 - g2\n",
    "for t in difference_graph:\n",
    "    print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** The difference operation is not symmetric."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Exercises3'></a>\n",
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.** Create an empty RDF graph using RDFLib. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = (...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.** Create RDF triples in RDFLib for the following triples in N-triples serialization.\n",
    "\n",
    "* $<http://example.org/science#Alan_Turing> <http://example.org/science#field> <http://example.org/science#ComputerScience> .$\n",
    "* $<http://example.org/science#Alan_Turing> <http://example.org/science#field> <http://example.org/science#Logic> .$\n",
    "* $<http://example.org/science#Alan_Turing> <http://example.org/science#bornYear> \"1938\"^^<http://www.w3.org/2001/XMLSchema#gYear> .$\n",
    "* $<http://example.org/science#Alan_Turing> <http://example.org/science#thesisTitle> \"Systems of Logic Based on Ordinals\"@en .$\n",
    "\n",
    "Add the triples to the graph created in Exercise 1 using RDFLib."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 3.** Given the following RDF literals in N-triples notation, create the literals using RDFLib. Make sure to specify `normalize=False` in RDFLib, to comply with the behavior of literals as defined in the RDF specification. \n",
    "* $1000.0^^<http://www.w3.org/2001/XMLSchema#integer>$ \n",
    "* $1000.0^^<http://www.w3.org/2001/XMLSchema#float>$\n",
    "* $1000.00^^<http://www.w3.org/2001/XMLSchema#float>$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 4.** Compare the literals created in Exercise 3. Which ones are different? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 5.** Below, we are loading the triples available at http://people.aifb.kit.edu/yt2652/swt/data/alan_turing.ttl. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h = rdflib.Graph()\n",
    "h.load(\"http://people.aifb.kit.edu/yt2652/swt/data/alan_turing.ttl\", format=\"ttl\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_metadata": {
   "author": "Maribel Acosta",
   "title": "Resource Description Framework - Jupyter Notebook",
   "year": "2019"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
