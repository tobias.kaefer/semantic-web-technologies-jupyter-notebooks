{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# SPARQL Query Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will learn about the foundations of SPARQL query evaluation or execution, i.e., how query answers are computed. The **input** for the evaluation is the the SPARQL query (or expression) and the RDF graph over which the expression is evaluated. The **output** of the evaluation process is the answers for the given query. \n",
    "\n",
    "We will focus on the evaluation of Basic Graph Patterns (BGPs) and the Join operator and we will learn about related conceps including solution mappings, result sets, and compatibility. \n",
    "\n",
    "This notebook is structured as follows:\n",
    "* [Evaluation of BGP Expressions](#Evaluation_of_BGP_Expressions)\n",
    "* [Result Sets and Solution Mappings](#Result_Sets_and_Solution_Mappings)\n",
    "* [Solution Mapping Compatibility](#Solution_Mapping_Compatibility)\n",
    "* [Join Evaluation](#Join_Evaluation)\n",
    "* [Exercises](#Exercises5)\n",
    "\n",
    "To handle RDF data, we are going to use the RDFLib library https://rdflib.readthedocs.io. To install this library execute the following command in the terminal `conda install -c conda-forge rdflib`. In particular, we will use the `algebra` and `evaluate` packages from RDFLib. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import rdflib\n",
    "from rdflib.plugins.sparql import algebra, evaluate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the remainder of this notebook, we are going to use the following RDF graph as example. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Graph as a turtle string\n",
    "ttl = \"\"\"\n",
    "@prefix ex: <http://example.org/baz#> .\n",
    "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . \n",
    "@prefix : <http://harth.org/astro-graph#> . \n",
    ":Sonne  ex:radius \"1.392e6\"^^xsd:double ; \n",
    "        ex:satellit :Merkur, :Venus, :Erde, :Mars ;\n",
    "        ex:name \"Sun\" .\n",
    ":Merkur ex:radius \"2439.7\"^^xsd:double . \n",
    ":Venus ex:radius \"6051.9\"^^xsd:double . \n",
    ":Erde ex:radius \"6372.8\"^^xsd:double ; \n",
    "      ex:satellit :Mond . \n",
    ":Mars ex:radius \"3402.5\"^^xsd:double ; \t  \n",
    "      ex:satellit :Phobos, :Deimos . \n",
    ":Mond ex:name \"Mond\"@de, \"Moon\"@en ; \n",
    "      ex:radius \"1737.1\"^^xsd:double . \n",
    ":Phobos ex:name \"Phobos\" . \n",
    ":Deimos ex:name \"Deimos\" . \n",
    "\"\"\"\n",
    "\n",
    "# Load the graph\n",
    "graph = rdflib.Graph().parse(data=ttl, format=\"turtle\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Evaluation_of_BGP_Expressions'></a>\n",
    "## Evaluation of BGP Expressions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we learned in the previous notebook *Querying RDF Data with SPARQL*, a Basic Graph Pattern (BGP) is a set of triple patterns. During the evaluation of a SPARQL query, BGPs are translated into **BGP algebra expressions**.  \n",
    "\n",
    "A BGP algebra expression is the atomic algebra expression according to the W3C SPARQL specification. The process to evaluate BGPs over an RDF graph is to perform **pattern matching**: BGPs are seen as graph templates where constants (or instantiations in the BGP) are matched over the RDF graph. \n",
    "\n",
    "An example of a BGP algebra expression is: \n",
    "\n",
    "```\n",
    "bgp = BGP(?x ex:radius ?r . ?x ex:name ?n .)\n",
    "```\n",
    "\n",
    "To create a BGP in RDFLib, first we create each triple pattern using `rdflib.URIRef` for URIs, `rdflib.Literal` for literals, and `rdflib.term.Variable` for variables. Please note that we do not use the symbols `?` or `$` when creating a variable term in RDFLib. \n",
    "\n",
    "For example, we will create a triple pattern `tp1` for `?x ex:radius ?r .`, and `tp2` for  `?x ex:name ?n .`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tp1 = [rdflib.term.Variable('x'), rdflib.URIRef('http://example.org/baz#radius'), rdflib.term.Variable('r')]\n",
    "tp2 = [rdflib.term.Variable('x'), rdflib.URIRef('http://example.org/baz#name'), rdflib.term.Variable('n')]\n",
    "print(tp1)\n",
    "print(tp2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can create a BGP algebra expression. In RDFLib, we use the function `algebra.BGP`, where the input `triples` corresponds to the list of triple patterns. \n",
    "\n",
    "**Important.** In RDFLib, in order to later evaluate the algebra expression, we need to store the created expression in the algebra attribute of the Python object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bgp = algebra.BGP(triples=[tp1, tp2])\n",
    "bgp.algebra = algebra.BGP(triples=[tp1, tp2])\n",
    "print(bgp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we evaluate the BGP using the `evaluate.evalQuery` function in RDFLib. The input of this function corresponds to the graph where the expression is evaluated, the algebra expression, and some initial bindings/solutions (which in our case are empty)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = evaluate.evalQuery(graph, bgp, initBindings={})\n",
    "for answer in results:\n",
    "    print(answer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The evaluation of the `bgp` algebra expression over `graph` produces two answers. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Result_Sets_and_Solution_Mappings'></a>\n",
    "## Result Sets and Solution Mappings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The set of answers produced by the evaluation of a SPARQL algebra expression is called a **result set**. In the literature, we find that result sets are typically denoted as $\\Omega$. \n",
    "\n",
    "Each of the answers produced in the evaluation of a SPARQL algebra expression is called a **solution mapping**. Formally, a solution mapping is defined as a partial function $\\mu$ from variables to RDF terms, i.e., $\\mu : V \\rightarrow U \\cup B \\cup L$. \n",
    "\n",
    "To better understand the concept of solution mapping, with the following Python function `to_dict`, we are going to translate the solution mappings produced by RDFLib in the `evaluate.evalQuery` into Python dictionaries where the RDF terms are serialized in N3. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from six import iteritems\n",
    "def to_dict(mu):\n",
    "    sol = {}\n",
    "    for x in iteritems(mu):\n",
    "        sol[x[0].n3()] = x[1].n3()\n",
    "    return sol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following, we execute again the `bgp` algebra expression over `graph`, and each of the solution mappings are translated using the `to_dict` function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = evaluate.evalQuery(graph, bgp, initBindings={})\n",
    "omega = []\n",
    "for answer in results:\n",
    "    omega.append(to_dict(answer))\n",
    "omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this new representation, it is easier to see how the variables are mappped (or bound) to RDF terms. For example,  in the second solution mapping, the variable `?n` is mapped to the RDF term `\"Moon\"@en`, denoted formally as $?n \\rightarrow \\textit{\"Moon\"@en}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **domain** of a solution mapping $\\mu$, denoted $dom(\\mu)$, corresponds to the set of variables for which the mapping is defined. \n",
    "\n",
    "If we represent solution mappings as Python dictionaries, we can obtain the domain of a solution mapping using the function `keys`. \n",
    "\n",
    "For example, the domain of the first solution mapping $\\mu_1$ in `omega` is $dom(\\mu1) = \\{?x, ?r, ?n\\}$. We compute it in Python as follows: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu1 = omega[0] # First element of omega\n",
    "mu1.keys()     # Get the domain of mu1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, if $x$ is a variable that belongs to the domain of a solution mapping $\\mu$, the operation $\\mu(x)$ returns the value of the variable $x$ in $\\mu$. \n",
    "\n",
    "If we represent solution mappings as Python dictionaries, we can perform $\\mu(x)$ by specifying the name of the variable $x$ that we want to access. For example: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu1['?n']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Solution_Mapping_Compatibility'></a>\n",
    "## Solution Mapping Compatibility"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two solution mappings $\\mu_1$, $\\mu_2$ are **compatible**, written $\\mu_1 \\sim \\mu_2$, if for all variable $x$ that belongs to $dom(\\mu_1) \\cap dom(\\mu_2)$ it holds that $mu_1(x) = mu_2(x)$. \n",
    "\n",
    "This means that two solution mappings are compatible if for all the variables that they have in common, those variables are mapped to the same RDF terms. \n",
    "\n",
    "For example, consider the following two solution mappings `mu3` and `mu4`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu3 = {'?x': '<http://harth.org/astro-graph#Mond>', '?r': '\"1737.1\"^^<http://www.w3.org/2001/XMLSchema#double>'}\n",
    "mu4 = {'?x': '<http://harth.org/astro-graph#Mond>', '?n': '\"Moon\"@en'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we identify the variables in common in the two solution mappings. To do this, we compute the intersection of the keys of `mu3` and `mu4` using the set intersection operator `&` in Python.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu3.keys() & mu4.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, there is only one variable in common. Now we can check whether the values of that variable are the same. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu3['?x'] == mu4['?x']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, the mappings `mu3` and `mu4` are compatible.  \n",
    "\n",
    "The following Python function returns `True` when two mappings are compatible, and `False` otherwise. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compatible(mu1, mu2):\n",
    "    compatible = True\n",
    "    for x in mu1.keys() & mu2.keys():\n",
    "        if mu1[x] != mu2[x]:\n",
    "            compatible = False\n",
    "    return compatible "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us know determine whether the following mapppings `mu5` and `mu6` are compatible using the `compatible` Python function that we just defined. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu5 = {'?x': '<http://harth.org/astro-graph#Mond>', '?r': '\"1737.1\"^^<http://www.w3.org/2001/XMLSchema#double>'}\n",
    "mu6 = {'?x': '<http://harth.org/astro-graph#Mond>', '?r': '\"1760.0\"^^<http://www.w3.org/2001/XMLSchema#double>'}\n",
    "compatible(mu5, mu6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question.** Are `mu5` and `mu6` compatible? Why? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another important operation is the **merging of compatible solution mapppings**. \n",
    "\n",
    "To illustrate the concept of merging compatible solution mappings, consider again the mappings `mu3` and `mu4` represented as Python dictionaries and which we have determined before that are compatible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu3 = {'?x': '\"<http://harth.org/astro-graph#Mond>\"', '?r': '\"1737.1\"^^<http://www.w3.org/2001/XMLSchema#double>'}\n",
    "mu4 = {'?x': '\"<http://harth.org/astro-graph#Mond>\"', '?n': '\"Moon\"@en'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assuming that solution mappings are represented as Python dictionaries, we can merge them using dictionary operations. First, we are going to create an empty dictionary and then update it with the contents of the solution mappings to combine. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu34 = {}        # Empty dictionary\n",
    "mu34.update(mu3) # Update\n",
    "mu34.update(mu4) # Update\n",
    "print(mu34)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result of merging two solution mappings is a solution mapping than contains the variables and terms from the merged mappings. Note that the variables in common appear only once in the merged mapping. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Join_Evaluation'></a>\n",
    "## Join Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**JOIN algebra expressions** represent the conjunction of two sub-algebra expressions. \n",
    "\n",
    "For example, consider the following SPARQL expression composed of two BGPs which are joined. \n",
    "\n",
    "```\n",
    "expr = JOIN(BGP(?x ex:radius ?r . ?x ex:name ?n .), BGP(?x ex:satellit ?s .)) \n",
    "```\n",
    "\n",
    "To evaluate a JOIN expression, first we evaluate the sub-expressions and then apply the **join operator**. In our example, that means that first we evaluate the two input BGPs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Triple patterns. \n",
    "tp1 = [rdflib.term.Variable('x'), rdflib.URIRef('http://example.org/baz#radius'), rdflib.term.Variable('r')]\n",
    "tp2 = [rdflib.term.Variable('x'), rdflib.URIRef('http://example.org/baz#name'), rdflib.term.Variable('n')]\n",
    "tp3 = [rdflib.term.Variable('x'), rdflib.URIRef('http://example.org/baz#satellit'), rdflib.term.Variable('s')]\n",
    "\n",
    "# BGPs.\n",
    "bgp1 = algebra.BGP(triples=[tp1, tp2])\n",
    "bgp1.algebra = algebra.BGP(triples=[tp1, tp2])\n",
    "\n",
    "bgp2 = algebra.BGP(triples=[tp3])\n",
    "bgp2.algebra = algebra.BGP(triples=[tp3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we evaluate the two BGPs `bgp1` and `bgp2`, and store the results in `omega_l` and `omega_r`, respectively. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results_l = evaluate.evalQuery(graph, bgp1, initBindings={})\n",
    "omega_l = []\n",
    "for answer in results_l:\n",
    "    omega_l.append(to_dict(answer))\n",
    "    \n",
    "results_r = evaluate.evalQuery(graph, bgp2, initBindings={})\n",
    "omega_r = []\n",
    "for answer in results_r:\n",
    "    omega_r.append(to_dict(answer))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we join the intermediate result sets $\\Omega_l$ and $\\Omega_r$ with a Join operator. Formally, the **Join operator** is defined as follows: \n",
    "\n",
    "$\\textit{Join}(\\Omega_l, \\Omega_r) = \\{ \\mu_l \\cup \\mu_r \\ | \\ \\mu_l \\in \\Omega_l, \\mu_r \\in \\Omega_r: \\mu_l \\sim \\mu_r\\}$\n",
    "\n",
    "Which means that, we have to compare each pair of solutions mappings from $\\Omega_l$ and $\\Omega_r$, and merge the solution mappings that are compatible. \n",
    "\n",
    "We can now implement the join operator in a Python function as follows. The inputs of the operator are the Python variables `omega_l` and `omega_r`, and the output is the result set `omega`. The join operator is as follows:  \n",
    "\n",
    "1. For each `mu_l` in `omega_l`\n",
    "2. For each `mu_r` in `omega_r`\n",
    "3. Check if `mu_l` and `mu_r` are compatible using the Python function `compatible` defined in the previous section\n",
    "4. If `compatible` returns `True`, then merge `mu_l` and `mu_r` with the steps defined in the previous section\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def join_operator(omega_l, omega_r): \n",
    "    omega = []\n",
    "    for mu_l in omega_l:\n",
    "        for mu_r in omega_r:\n",
    "            if compatible(mu_l, mu_r):\n",
    "                mu = {}\n",
    "                mu.update(mu_l)\n",
    "                mu.update(mu_r)\n",
    "                omega.append(mu)\n",
    "    return omega      "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can apply the `join_operator` function to the result sets `omega_l` and `omega_r` (the results of evaluating the BGP algebra expressions `bgp1` and `bgp2`, respectively). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = join_operator(omega_l, omega_r)\n",
    "for mu in omega:\n",
    "    print(mu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Exercises5'></a>\n",
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the following RDF graph in the Turtle serialization. Formulate the SPARQL queries to answer the questions presented below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Graph as a turtle string\n",
    "ttl = \"\"\"\n",
    "@prefix ex: <http://example.org/baz#> .\n",
    "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . \n",
    "@prefix : <http://harth.org/astro-graph#> . \n",
    ":Sonne  ex:radius \"1.392e6\"^^xsd:double ; \n",
    "        ex:satellit :Merkur, :Venus, :Erde, :Mars ; \n",
    "        ex:name \"Sun\" .\n",
    ":Merkur ex:radius \"2439.7\"^^xsd:double . \n",
    ":Venus ex:radius \"6051.9\"^^xsd:double . \n",
    ":Erde ex:radius \"6372.8\"^^xsd:double ; \n",
    "      ex:satellit :Mond . \n",
    ":Mars ex:radius \"3402.5\"^^xsd:double ; \t  \n",
    "      ex:satellit :Phobos, :Deimos . \n",
    ":Mond ex:name \"Mond\"@de, \"Moon\"@en ; \n",
    "      ex:radius \"1737.1\"^^xsd:double . \n",
    ":Phobos ex:name \"Phobos\" . \n",
    ":Deimos ex:name \"Deimos\" . \n",
    "\"\"\"\n",
    "\n",
    "# Load the graph\n",
    "graph = rdflib.Graph().parse(data=ttl, format=\"turtle\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 1.** Write the following SPARQL algebra expressions using the RDFLib library. \n",
    "* `BGP(?x ex:name \"Sun\" .)`\n",
    "* `BGP(?s ex:radius ?o .)`\n",
    "* `BGP(:Mars ex:satellit ?s . ?s ex:name ?n .)`\n",
    "* `BGP(?x ex:satellit ?s1 . ?x ex:satellite ?s2 .)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tp1 = (...)\n",
    "tp2 = (...)\n",
    "(...)\n",
    "bgp1 = (...)\n",
    "(...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2.** Evaluate the BGPs created in Question 1 over the graph stored in `graph` using the `evaluate.evalQuery` from RDFLib.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.** Evaluate the following expressions: \n",
    "* `JOIN(BGP(?x ex:name \"Sun\" .), BGP(?x ex:satellit ?s1 . ?x ex:satellite ?s2 .))`\n",
    "* `JOIN(BGP(:Mars ex:satellit ?s . ?s ex:name ?n .), BGP(?s ex:radius ?o .))`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4 (Difficult).** Similar to the function `join_operator`, complete the Python function `union_operator` that implements the execution of the SPARQL $Union$ algebra operator. \n",
    "\n",
    "$Union(\\Omega_l, \\Omega_r) = \\{ \\mu \\ | \\ \\mu \\in \\Omega_l \\ or \\ \\mu \\in \\Omega_r \\}$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def union_operator(omega_l, omega_r):\n",
    "    omega = []\n",
    "    (...)\n",
    "    return omega "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5.** Evaluate the following expression using the function `union_operator` from Question 4. \n",
    "\n",
    "```UNION(BGP(:Mars ex:satellit ?s .), BGP(:Erde ex:satellit ?s .))```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_metadata": {
   "author": "Maribel Acosta",
   "title": "SPARQL Query Evaluation - Jupyter Notebook",
   "year": "2019"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
