{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Querying RDF Data with SPARQL"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will learn about the SPARQL query language. SPARQL is the W3C recommendation to formulate qureies over RDF graphs. \n",
    "\n",
    "This notebook is structured as follows:\n",
    "* [SELECT Query Form](#SELECT_Query_Form)\n",
    "* [Triple Patterns](#Triple_Patterns)\n",
    "* [Basic Graph Patterns](#Basic_Graph_Patterns)\n",
    "* [Union](#Union)\n",
    "* [Optional](#Optional)\n",
    "* [Filter](#Filter)\n",
    "* [Exercises](#Exercises4)\n",
    "\n",
    "To handle RDF data, we are going to use the `rdflib` library https://rdflib.readthedocs.io. To install this library execute the following command in the terminal `conda install -c conda-forge rdflib`.\n",
    "\n",
    "**Important.** Please note that RDFLib does not completely implement the SPARQL Query Language, e.g. the COUNT Keyword is not supported. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from rdflib import Graph"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='SELECT_Query_Form'></a>\n",
    "## SELECT Query Form"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SPARQL supports four query forms:\n",
    "* **SELECT**: Return all or a subset of the solution mappings\n",
    "* **ASK**: Return true or false, depending on whether there is a solution mapping or graph pattern\n",
    "* **CONSTRUCT**: Return a set of triples/a graph, where the mappings are filled into a specific graph pattern template\n",
    "* **DESCRIBE**: Return a set of triples / a graph that describes a certain resource (URI)\n",
    "\n",
    "In this notebook, we are concern with the SELECT query form. \n",
    "\n",
    "The basic structure of a *SELECT query* is as follows: \n",
    "\n",
    "```SELECT <variables to return or project> \n",
    "FROM <RDF graph to query>\n",
    "WHERE { \n",
    "    <Matching conditions>\n",
    "}```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In `rdflib`, we will not use the `FROM` clause when writing SPARQL queries. Instead, we are going to create and load data into an RDF graph. Then, we are going to execute the query over the created python structure. \n",
    "\n",
    "In the remainder of this notebook, we are going to use the following RDF graph as example. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Graph as a turtle string\n",
    "ttl = \"\"\"\n",
    "@prefix ex: <http://example.org/baz#> .\n",
    "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . \n",
    "@prefix : <http://harth.org/astro-graph#> . \n",
    ":Sonne  ex:radius \"1.392e6\"^^xsd:double ; \n",
    "        ex:satellit :Merkur, :Venus, :Erde, :Mars . \n",
    ":Merkur ex:radius \"2439.7\"^^xsd:double . \n",
    ":Venus ex:radius \"6051.9\"^^xsd:double . \n",
    ":Erde ex:radius \"6372.8\"^^xsd:double ; \n",
    "      ex:satellit :Mond . \n",
    ":Mars ex:radius \"3402.5\"^^xsd:double ; \t  \n",
    "      ex:satellit :Phobos, :Deimos . \n",
    ":Mond ex:name \"Mond\"@de, \"Moon\"@en ; \n",
    "      ex:radius \"1737.1\"^^xsd:double . \n",
    ":Phobos ex:name \"Phobos\" . \n",
    ":Deimos ex:name \"Deimos\" . \n",
    "\"\"\"\n",
    "\n",
    "# Load the graph\n",
    "graph = Graph().parse(data=ttl, format=\"turtle\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Triple_Patterns'></a>\n",
    "## Triple Patterns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Triple patterns** are similar to RDF triples but with variables in any position (subject, predicate, object). In SPARQL, variables are indicated with the prefix `?` or `$`. \n",
    "\n",
    "Using the data loaded into `graph`, the following triple pattern asks for the radius of the Earth: `:Erde ex:radius ?r .`. In the following, we show how to formulate a SPARQL query using this triple pattern. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tp = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?r\n",
    "WHERE \n",
    "{\n",
    "    :Erde ex:radius ?r .\n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variable `query` is a string with the specified query. To evaluate the query over the data in `graph`, we use the `rdflib` function `query` as follows. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = graph.query(tp)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The answers of the query are stored in the variable `results`. Then, we can iterate over the results and print them. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Basic_Graph_Patterns'></a>\n",
    "## Basic Graph Patterns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Basic Graph Patterns (BGPs) are set of triple patterns. **BGPs represent conjunctions of conditions.** \n",
    "\n",
    "For example, to ask for the radius of the Earth *and* its satellites we use a BGP composed of triple paterns: \n",
    "\n",
    "```:Erde ex:radius ?r .\n",
    "   :Erde ex:satellit ?s . ```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bgp1 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?r ?s \n",
    "WHERE \n",
    "{\n",
    "    :Erde ex:radius ?r .\n",
    "    :Erde ex:satellit ?s .\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(bgp1)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** Please note that, when several variables are projected in the `SELECT` part, `rdflib` prints the values in the same order as of the projected variables.    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in the previous BGP, the triple patterns did not have variables in common.  However, triple patterns in BGPs can also have variables in common which are bound to the same term. \n",
    "\n",
    "For example, assume that we want to ask for the satellites of Mars and the name of those satellies. This question can be formulated as follows: \n",
    "```:Mars ex:satellit ?x . \n",
    "   ?x ex:name ?n .```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bgp2 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?x ?n\n",
    "WHERE \n",
    "{\n",
    "    :Mars ex:satellit ?x . \n",
    "    ?x ex:name ?n .\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(bgp2)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Union'></a>\n",
    "## Union"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In SPARQL, **union represents disjunction of conditions**. \n",
    "\n",
    "For example, to ask for the satellites of the Earth *or* the satellites of Mars, we use a union as follows: \n",
    "\n",
    "```{:Erde ex:satellit :?s .} \n",
    "   UNION \n",
    "   {:Mars ex:satellit ?s .}```\n",
    "\n",
    "**Important.** The union operator is applied to the expressions before and after the keyword `UNION`. To avoid ambiguities, make sure to enclose these expressions in curly brackets.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "union1 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?s\n",
    "WHERE \n",
    "{\n",
    "    {:Erde ex:satellit ?s .} \n",
    "    UNION\n",
    "    {:Mars ex:satellit ?s .}\n",
    "\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(union1)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we use the same variables in the disjunctive expressions, i.e., the expressions are *union-compatible*. \n",
    "\n",
    "In SPARQL, it is possible to write *non union-compatible* expressions in the union. This is useful in cases when we want to know the expressions that matched the produced answers. \n",
    "\n",
    "For example, in the following query we will use two different variables in the union expressions (and in the SELECT part of the query). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "union2 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?s1 ?s2 \n",
    "WHERE \n",
    "{\n",
    "    {:Erde ex:satellit ?s1 .} \n",
    "    UNION\n",
    "    {:Mars ex:satellit ?s2 .}\n",
    "\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(union2)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question.** What is the difference between the results produced by the queries in variables `union1` and `union2`?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Optional'></a>\n",
    "## Optional"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In SPARQL, an **optional represents optional/additional conditions that can be fulfilled**. \n",
    "\n",
    "For example, to ask for the radius of celestial bodies and *optionally* their satellites (in case they have), we can formulate the following optional expression: \n",
    "\n",
    "```?x ex:radius ?y . \n",
    "    OPTIONAL\n",
    "    {?x ex:satellit ?z .}\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optional = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?x ?y ?z \n",
    "WHERE \n",
    "{\n",
    "    ?x ex:radius ?y . \n",
    "    OPTIONAL\n",
    "    {?x ex:satellit ?z .}\n",
    "\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(optional)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** Please note that in some results, the values for the variable `?z` (specified in the optional condition)  corresponds to `None`. In `rdflib`, this means that the variables in the optional condition were not matched. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Filter'></a>\n",
    "## Filter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Filter allows for specifying conditions that the solutions of triple patterns must fulfilled. \n",
    "\n",
    "In filter expressions, we can specify:\n",
    "* **Comparison operators (<, =, >, <=, >=, !=)**: usable on numeric types or booleans\n",
    "* **Aritmethic operators (+, -, *, /)**: usable on numeric types \n",
    "* **Boolean operators (AND (&&), OR (||), NOT (!))**: to combine boolean sub-expressions \n",
    "\n",
    "For example, to ask for the astronomical objects with radius lower than 6000 but higher than 2000, we can formulate the following pattern with a filter expression: \n",
    "\n",
    "``` ?x ex:radius ?y .  FILTER (?y < 6000 && ?y > 2000)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter1 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?x ?y\n",
    "WHERE \n",
    "{\n",
    "    ?x ex:radius ?y . \n",
    "    FILTER (?y < 6000)\n",
    "\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(filter1)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Functions in Filter Expressions\n",
    "In filter expressions, it is also possible to apply built-in functions  (https://en.wikibooks.org/wiki/SPARQL/Expressions_and_Functions#Functions). \n",
    "\n",
    "For example, to filter literals based on their language tags, we can apply the function `LANG`. The following filter expressions allows for producing answers where the name of the astronomical objects is in English (specified as `en`): \n",
    "\n",
    "```?x ex:name ?n . FILTER (LANG(?n) = 'en')```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter2 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT ?x ?n\n",
    "WHERE \n",
    "{\n",
    "    ?x ex:name ?n . \n",
    "    FILTER (LANG(?n) = 'en')\n",
    "\n",
    "}\n",
    "\"\"\"\n",
    "results = graph.query(filter2)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question.** How does the FILTER LANG expression behave in literals with no language tags?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Exercises4'></a>\n",
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the following RDF graph in the Turtle serialization. Formulate the SPARQL queries to answer the questions presented below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Graph as a turtle string\n",
    "ttl = \"\"\"\n",
    "@prefix ex: <http://example.org/baz#> .\n",
    "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . \n",
    "@prefix : <http://harth.org/astro-graph#> . \n",
    ":Sonne  ex:radius \"1.392e6\"^^xsd:double ; \t \n",
    "        ex:satellit :Merkur, :Venus, :Erde, :Mars . \n",
    ":Merkur ex:radius \"2439.7\"^^xsd:double . \n",
    ":Venus ex:radius \"6051.9\"^^xsd:double . \n",
    ":Erde ex:radius \"6372.8\"^^xsd:double ; \n",
    "      ex:satellit :Mond . \n",
    ":Mars ex:radius \"3402.5\"^^xsd:double ; \t  \n",
    "      ex:satellit :Phobos, :Deimos . \n",
    ":Mond ex:name \"Mond\"@de, \"Moon\"@en ; \n",
    "      ex:radius \"1737.1\"^^xsd:double . \n",
    ":Phobos ex:name \"Phobos\" . \n",
    ":Deimos ex:name \"Deimos\" . \n",
    "\"\"\"\n",
    "\n",
    "# Load the graph\n",
    "graph = Graph().parse(data=ttl, format=\"turtle\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 1.** Retrieve astronomical objects that are satellites of the sun (:Sonne). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query1 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT (...)\n",
    "WHERE \n",
    "{\n",
    "    (...)\n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = graph.query(query1)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2.** Retrieve astronomical objects of our Solar System. An astronomical object is defined as the satellites of the sun (:Sonne), or the objects that orbit around the satellites of the Sun (:Sonne). \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query2 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT (...)\n",
    "WHERE \n",
    "{\n",
    "    (...)\n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = graph.query(query2)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.** Retrieve objects with a volume of more than 2 x 1010(km3). Objects with specified radius can be assumed as sphere, that is, V = 4/3πr3. π can be expanded as 3.14159. Also, if available, include the objects of which they are a satellite of."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query3 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT (...)\n",
    "WHERE \n",
    "{\n",
    "    (...)\n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = graph.query(query3)\n",
    "for result in results:\n",
    "    print(f\"?x = <{result[0]}>; ?s = <{result[1]}>\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4.** Objects with a satellite for which the satellite has an English name, and that are also satellite of an object of more than 3000 (km) in diameter. Diameter is computed as 2 times the radius. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query4 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT (...)\n",
    "WHERE \n",
    "{\n",
    "    (...)\n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = graph.query(query4)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5.** Retrieve objects with at least two satellites; assume that different URIs denote different objects. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query5 = \"\"\"\n",
    "PREFIX ex:  <http://example.org/baz#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "PREFIX : <http://harth.org/astro-graph#>  \n",
    "SELECT (...)\n",
    "WHERE \n",
    "{\n",
    "    (...) \n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = graph.query(query5)\n",
    "for result in results:\n",
    "    print(result)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_metadata": {
   "author": "Maribel Acosta",
   "title": "Querying RDF Data with SPARQL - Jupyter Notebook",
   "year": "2019"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
