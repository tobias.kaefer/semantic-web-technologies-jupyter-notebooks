{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Foundations of the Web"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will learn about core concepts of the Web. In particular, we will learn about basic HTTP operations. \n",
    "\n",
    "This notebook is structured as follows:\n",
    "* [Dereferencing URIs](#Dereferencing_URIs)\n",
    "* [Content Negotiation](#Content_Negotiation)\n",
    "* [Resource vs. Information Resource](#Resource_vs._Information_Resource) \n",
    "* [Exercises](#Exercises1)\n",
    "\n",
    "We are going to use the `requests` Python library http://docs.python-requests.org to perform HTTP operations. Please note that there are other Python libraries (e.g., [httplib](https://docs.python.org/2/library/httplib.html)) that also support HTTP. \n",
    "\n",
    "To handle URIs, we are going to use the `urllib.parse` Python 3 library. If you are using Python 2, change the import line as follows `import urlparse`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "import urllib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Dereferencing_URIs'></a> \n",
    "## Dereferencing URIs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an example, we are going to dereference the RDF document that describes the friends of [Tim Berners-Lee](https://en.wikipedia.org/wiki/Tim_Berners-Lee)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri = \"https://timbl.com/timbl/Public/friends.ttl#i\" "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To dereference a URI, we use the method `get` of the `requests` Python library. The output of the request in stored into the variable `response`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response = requests.get(uri)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we can check the HTTP status code of the response."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response.status_code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, we can obtain the content of the response. We are using the Python function `print`to render the string of the content in a readable way. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(response.text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Content_Negotiation'></a> \n",
    "## Content Negotiation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Content negotiation is a mechanism supported by the HTTP protocol to serve different versions of a document under the same URI."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dereferencing URIs with default content type"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an example, we are going to use a URI from DBpedia http://dbpedia.org. First, we are going to dereference the URI without specifiying a desired content type, i.e., we are going to dereference the URI in the format served by default. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important.** To illustrate the full process of serving URIs with conneg, we are going to turn off the automatic redirects in the request `get` method. To do this, we specifify `allow_redirects=False`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dbpedia_uri = \"http://dbpedia.org/resource/Marie_Curie\" \n",
    "response_plain = requests.get(dbpedia_uri, allow_redirects=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we check the status code of the response.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_plain.status_code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The status code indicates a redirection. To obtain the destination URI, we inspect the field `Location` in the response header.     "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_plain.headers['Location']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that we need to execute another HTTP request, now aiming at the specified location. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_redirect = requests.get(response_plain.headers['Location'], allow_redirects=False)\n",
    "response_redirect.status_code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The request on the redirected URI is now `200`. This means that we have dereferenced the desired URI. To obtain the format of the response, we can inspect the field `Content-Type` in the response header."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_redirect.headers['Content-Type']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, the DBpedia server returns content in HTML. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dereferencing URIs with a desired content type"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To perform content negotiation with the DBpedia server, we can set the desired response format in the `Accept` field of the request header. In this example, we want to retrieve the content in JSON, which is specified as `application/json`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "headers = {'Accept': 'application/json'}\n",
    "response_conneg = requests.get(dbpedia_uri, headers=headers, allow_redirects=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we verify that the status code of the response is `303` and then we obtain the location of the redirected URI. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(response_conneg.status_code)\n",
    "print(response_conneg.headers['Location'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we perform an HTTP request on the destination URI. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_conneg_redirect = requests.get(response_conneg.headers['Location'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following, we check the content of the response by inspecting the reponse header and obtain the content of the response. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(response_conneg_redirect.headers['Content-Type'])\n",
    "print(response_conneg_redirect.text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have dereferenced the URI in JSON format from the DBpedia server. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Resource_vs._Information_Resource'></a> \n",
    "## Resource vs. Information Resource"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "URIs may identify resources or information resources. In this lecture, we use the term **resource** as a name (or identifier) for a thing. An **information resource** contains machine-readable data that describe an entity.\n",
    "\n",
    "In this section of the notebook, we are going to identify whether a given URI corresponds to an  **information resource**. To do this, we need to distinguish between hash URIs and slash URIs. \n",
    "\n",
    "A hash URI contains a `#`, which immediately indicates that this URI is used as a name for a thing. In other words, **hash URIs are always resources**. To obtain the information resource associated with a hash URI, we just have to strip (remove) the fragment of the URI (the part after the `#`). \n",
    "\n",
    "A slash URI contains `/` and no hashes. In the case of slash URIs, we need to dereference the URI and inspect the response header: \n",
    "* If the status code indicates a **redirect, then the slash URI is a resource**. \n",
    "* If the status code indicates **success, then the slash URI is an information resource**. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example with hash URI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets try the following example with the URI stored in the variable `uri`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri = \"https://timbl.com/timbl/Public/friends.ttl#i\" "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To automatically check if `uri` is a hash or a slash URI, we can use the function `urldefrag` from the `urllib.parse` library. This function returns a 2-tuple: the first element corresponds to the first part of the URI (the part before the `#`, if exists); the second element corresponds to the fragment of the URI (the part after the `#`, if exists). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(uri_path, fragment) = urllib.parse.urldefrag(uri)\n",
    "print(uri_path)\n",
    "print(fragment)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To determine if the URI is a hash URI we can check whether the fragment part is empty (the empty string in Python is represented as `''`). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fragment is ''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fragment is not none, this indicates that `uri` is a hash URI and, therefore, is also a resource. \n",
    "\n",
    "The information resource associated with `uri` is the URI stored in the variable `uri_path`.    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(uri_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example with slash URI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets try this example with the URI stored in the variable `dbpedia_uri`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dbpedia_uri = \"http://dbpedia.org/resource/Marie_Curie\" "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly to the example with hash URIs, we check whether the given URI is a hash URI by applying the `urldefrag` function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(dbpedia_uri_path1, dbpedia_fragment) = urllib.parse.urldefrag(dbpedia_uri)\n",
    "dbpedia_fragment is ''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fragment of the URI is empty, which means that `dbpedia_uri` is a slash URI. \n",
    "\n",
    "Then, we have to determine whether `dbpedia_uri` is a resource or infomation resource by looking up the URI and inspecting the response. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_plain = requests.get(dbpedia_uri, allow_redirects=False)\n",
    "response_plain.status_code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The status code indicates a redirect, therefore `dbpedia_uri` corresponds to a resource.\n",
    "\n",
    "To obtain the information resource associated with `dbpedia_uri`, we can dereference the URI and follow all the redirects (setting `allow_redirects=True` in the `get` function). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_redirect = requests.get(dbpedia_uri, allow_redirects=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After following the redirects, we check the status code of the last visited URI. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_redirect.status_code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, the information resource associated with `uri_dbpedia` is stored in the `url` attribute of the response.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_redirect.url"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Exercises1'></a>\n",
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.** Complete the following code to dereference the URI `http://dbpedia.org/resource/Karlsruhe` in JSON format. Set the `get` method of the `requests` library to follow the redirects automatically. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri1 = ...\n",
    "headers1 = {'Accept': ...}\n",
    "response1 = requests.get(uri1, headers=headers1, allow_redirects=...) \n",
    "print(response1.text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.** Complete the following code to obtain the status code of `response1` used in Exercise 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response1...."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 3.** Complete the following code to dereference the URI `http://dbpedia.org/resource/Eiffel_Tower` in the format N-triples using the `requests` library. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri3 = ...\n",
    "headers3 = ...\n",
    "response3 = ...\n",
    "print(response3.text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 4 (Difficult).** Given the following four URIs, determine whether they are resources or information resources. For the resources, obtain the associated information resource.    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uri1 = \"http://dbpedia.org/ontology/Scientist\"\n",
    "uri2 = \"http://people.aifb.kit.edu/yt2652/swt/data/nobel.ttl#Marie_Curie\"\n",
    "uri3 = \"http://dbpedia.org/data/Karlsruhe.n3\"\n",
    "uri4 = \"http://dbpedia.org/page/Alan_Turing\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_metadata": {
   "author": "Maribel Acosta",
   "title": "Foundations of the Web - Jupyter Notebook",
   "year": "2019"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
